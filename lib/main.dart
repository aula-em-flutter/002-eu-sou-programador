// ! Biblioteca necessária para o sistema
import 'package:flutter/material.dart';

// ! Função principal do sistema em Dart
void main() => runApp(const MyApp());

// ! Criação de uma classe com extensão "StatelessWidget"
class MyApp extends StatelessWidget {
  // ! Inicialização da função
  const MyApp({super.key});

  // ! ...
  @override
  // ! ...
  Widget build(BuildContext context) {
    // ! Retorno de uma função "MaterialApp" do material design
    return const MaterialApp(
      // ! Chamado da função "Home"
      home: Home(),
    );
  }
}

// ! Criação da função "Home" com extensão "StatelessWidget"
class Home extends StatelessWidget {
  // ! Inicialização da função
  const Home({super.key});

  //! ...
  @override
  // ! ...
  Widget build(BuildContext context) {
    // ! Retorno da widget "Scaffold"
    return Scaffold(
      // ! Criação e configuração do widget "AppBar"
      appBar: AppBar(
        // ! Colocar uma cor no widget "AppBar"
        backgroundColor: Colors.cyan[900],
        // ! Colocar um titulo no widget "AppBar"
        title: const Text('Eu Sou Programador'),
        // ! Centralizar o texto do widget "AppBar"
        centerTitle: true,
        // ! Formatação do texto do widget "AppBar"
        titleTextStyle: const TextStyle(
          // ! Cor do texto
          color: Colors.white,
          // ! Modelo de fonte para o texto
          fontFamily: 'american',
          // ! Tamanho da fonte
          fontSize: 24.0,
          // ! Coloca a fonte em negrito
          fontWeight: FontWeight.bold,
        ),
      ),
      // ! Coloca uma cor para o widget "Scaffold"
      backgroundColor: Colors.teal[500],
      // ! Criação de um widget para o corpo do sistema
      body: const Column(
        // ! ...
        children: <Widget>[
          // ! Coloca uma imagem no corpo do sistema
          Image(
            // ! Carrega a imagem de um local interno
            image: AssetImage('assets/image/home.png'),
            // ! Configuração do tamanho da imagem
            width: 400.0,
            height: 400.0,
          ),
          // ! Adiciona um widget de texto no corpo do sistema
          Text(
            'Software',
            // ! Formatação do texto
            style: TextStyle(
                color: Colors.white, fontFamily: 'american', fontSize: 50.0),
          ),
          // ! Adiciona um widget de texto no corpo do sistema
          Text(
            'Engineer',
            // ! Formatação do texto
            style: TextStyle(
                color: Colors.white, fontFamily: 'american', fontSize: 50.0),
          ),
        ],
      ),
    );
  }
}
